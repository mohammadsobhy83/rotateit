// External Dependencies
import React, { Component } from 'react';


// Internal Dependencies
import './style.css';


class HelloWorld extends Component {

  static slug = 'raex_hello_world';
  
  
  static css(props) {
    
              
        //default vars
        let perspective = props.perspective;
        let rotateX = props.rotate_x;
        let rotateY = props.rotate_y;
        let rotateZ = props.rotate_z;

        //hover vars
        let hover_perspective = props.rotate_perspective__hover_enabled === 'on' ? props.rotate_perspective__hover : props.perspective;

        let hover_rotateX = props.rotate_x__hover_enabled === 'on' ? props.rotate_x__hover : props.rotate_x;

        let hover_rotateY = props.rotate_y__hover_enabled === 'on' ? props.rotate_y__hover : props.rotate_y;

        let hover_rotateZ = props.rotate_z__hover_enabled === 'on' ? props.rotate_z__hover : props.rotate_z;

        var css = [];

        css.push([{
            selector: "%%order_class%% .wrapper",
            declaration: `transform: perspective(${perspective}) 
                          rotateX(${rotateX})
                          rotateY(${rotateY})
                           rotateZ(${rotateZ});`
        }]);

       css.push([{
           selector: "%%order_class%% .wrapper:hover",
           declaration: `transform: perspective(${hover_perspective})
                                    rotateX(${hover_rotateX})
                                    rotateY(${hover_rotateY})
                                    rotateZ(${hover_rotateZ});`
        } ]);
    


    return css;
  };

  render() {
    

    return (
      <div className='wrapper'>
       <img src={this.props.image}></img>
        
      </div>
    );
  }
}

export default HelloWorld;

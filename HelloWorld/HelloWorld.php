<?php

class RAEX_HelloWorld extends ET_Builder_Module {

	public $slug       = 'raex_hello_world';
	public $vb_support = 'on';

	protected $module_credits = array(
		'module_uri' => '',
		'author'     => 'OXYGEN',
		'author_uri' => '',
	);

	public function init() {
		$this->name = esc_html__( 'Hello World', 'raex-range-extension' );
	}

	public function get_fields() {
		return array(
			'image'     => array(
				'label'     => esc_html__( 'My plugin', 'simp-simple-extension' ),
				'type'            => 'upload',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'upload your image 1', 'simp-simple-extension' ),
				'toggle_slug'     => 'main_content',
				'choose_text' => esc_html__( 'choose image', 'et_builder' ),
				'upload_button_text' => esc_html__( 'upload your image ', 'et_builder' ),
				'update_text' => esc_html__( 'update text', 'et_builder' ),
				'data_type' => 'image',
				'hover' => 'tabs'
			),

			'perspective' => array(
				//start basic
					'label'           => esc_html__( 'Perspective', 'fimo-first-module' ),
					'type'            => 'range',
					'option_category' => 'basic_option',
					'toggle_slug' => 'main_content', 
				//end basic
				//start all settings
					'range_setting' => array (
						'step' => '1px',
						'min' => '0',
						'max' => '200px'
								),
						'fixed_unit' => 'px',
						'default' => '0px',
						'hover' => 'tabs'	
						),
					
		'rotate_x' => array(
			//start basic
				'label'           => esc_html__( 'Rotate x', 'fimo-first-module' ),
				'type'            => 'range',
				'option_category' => 'basic_option',
				'toggle_slug' => 'main_content', 
			//end basic
			//start all settings
				'range_setting' => array (
					'step' => '1deg',
					'min' => '-90',
					'max' => '+90'
										),
				'fixed_unit' => 'deg',
				
				'default' => '0deg',
				'hover' => 'tabs'				
					),

	'rotate_y' => array(
		//start basic
			'label'           => esc_html__( 'Rotate y', 'fimo-first-module' ),
			'type'            => 'range',
			'option_category' => 'basic_option',
			'toggle_slug' => 'main_content', 
		//end basic
		//start all settings
			'range_setting' => array (
				'step' => '1deg',
				'min' => '-90',
				'max' => '+90'
									),
			'fixed_unit' => 'deg',
			'default' => '0deg',
			'hover' => 'tabs'
			),

'rotate_z' => array(
	//start basic
		'label'           => esc_html__( 'Rotate z', 'fimo-first-module' ),
		'type'            => 'range',
		'option_category' => 'basic_option',
		'toggle_slug' => 'main_content', 
	//end basic
	//start all settings
		'range_setting' => array (
			'step' => '1deg',
			'min' => '-90',
			'max' => '+90'
								),
		'fixed_unit' => 'deg',
		'default' => '0deg',
		'hover' => 'tabs'
			),
		);	
	}

	public function get_advanced_fields_config() {

		

	}

	public function render( $attrs, $content = null, $render_slug ) {
		
		//default vars

		$perspective = $this->props["perspective"]; //props.perspective
		$rotateX = $this->props["rotate_x"]; //props.rotate_x
		$rotateY = $this->props["rotate_y"]; //props.rotate_y
		$rotateZ = $this->props["rotate_z"]; //props.rotate_z

		//hover vars

		$hover_perspective = $this->props["rotate_perspective__hover_enabled"] === 'on' ? $this->props["rotate_perspective__hover"] : $this->props["perspective"];

		$hover_rotateX = $this->props["rotate_x__hover_enabled"] === 'on' ? $this->props["rotate_x__hover"] : $this->props["rotate_x"];

		$hover_rotateY = $this->props["rotate_y__hover_enabled"] === 'on' ? $this->props["rotate_y__hover"] : $this->props["rotate_y"];

		$hover_rotateZ = $this->props["rotate_z__hover_enabled"] === 'on' ? $this->props["rotate_z__hover"] : $this->props["rotate_z"];



						  
		ET_Builder_Element::set_style( $render_slug, array(
				"selector"   => "%%order_class%% .wrapper",
				"declaration" => "transform: perspective({$perspective})
											 rotateX({$rotateX})
											 rotateY({$rotateY})
											 rotateZ({$rotateZ})",
								  ) );

		
						  
		ET_Builder_Element::set_style( $render_slug, array(
				"selector"    => "%%order_class%% .wrapper:hover",
				"declaration" => "transform: perspective({$hover_perspective} 
											 rotateX({$hover_rotateX}) 
											 rotateY({$hover_rotateY} 
											 rotateZ({$hover_rotateZ})",
								  ) );



		

		
		return sprintf( 
			 '<div class="wrapper">
				<div><img src=%1$s></img></div>
			  </div>',
			$this-> props["image"],
			et_builder_is_hover_enabled("image" , $this->props)
			
						);
		}
}

new RAEX_HelloWorld;
